Node.prototype.childElement = function(n) {
  let childs = this.children;
  let selected;

  if (typeof n == "number") {
    // select and return the nth child element
    selected = childs[n-1];
  }
  else if (typeof n == "string") {
    // select and return every nth child element
    n = parseInt(n);
    selected = [];
    for (let i=n-1; i<childs.length; i+=n) {
      selected.push(childs[i]);
    }
  }

  selected.css = function (style, value) {
    if (Array.isArray(selected)) {
      selected.forEach( (node)=> {
        node.style.setProperty(style, value);
      });
    }
    else {
      selected.style.setProperty(style, value);
    }
  }

  return selected;
  //return new Wrapper(selected);
} // end childElement


/*
class Wrapper {

  constructor(nodes) {
    if(Array.isArray(nodes)) this.nodes = nodes;
    else this.nodes = [nodes];
  } // end constructor

  css (style, value) {
    this.nodes.forEach( (node)=> {
      node.style.setProperty(style, value);
    });
  } // end css

} // end Wrapper
*/



$(document).ready( ()=> {

  // do it with javascript
  $("#javascript").on("click", function() {
    document.getElementById("parent").childElement(2).css("background-color", "pink");
    document.getElementById("parent").childElement("3.").css("color", "#f91200");
  });

  // do it with jQuery
  $("#jQuery").on("click", function() {
    $("#parent > div:nth-child(2)").css("background-color", "#b8b8ea");
    $("#parent > div:nth-child(3n)").css("color", "white");
  });



  K("#parent > div:nth-child(2)").css("background-color", "lime");
  K("#parent > div:nth-child(3n)").css("color", "orange");

});




this["K"] = function(selector) {
  let find_nodes = document.querySelectorAll(selector);
  let my_nodes = Array.prototype.slice.call(find_nodes);

  my_nodes.css = function(style, value) {
    my_nodes.forEach( (node)=> {
      node.style.setProperty(style, value);
    });
  }

  return my_nodes;
}
