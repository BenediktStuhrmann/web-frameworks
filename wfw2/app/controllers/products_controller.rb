class ProductsController < ApplicationController
  def show
  end

  def show_detail
    params.require :product_id
    @product = Product.find(params[:product_id])
  end

  def create
    if (params.has_key?(:name) && params.has_key?(:product_number) && params.has_key?(:price))
      Product.create(:name => params[:name], :product_number => params[:product_number], :price => params[:price])
      redirect_to products_show_path
    end
  end

  def modify
    params.require :product_id
    @product = Product.find(params[:product_id])

    if (params.has_key? :modify)
      @product.name = params[:name]
      @product.product_number = params[:product_number]
      @product.price = params[:price]
      @product.save
      redirect_to products_show_path
    end
  end

  def delete
    if (params.has_key? :product_id)
      product = Product.find(params[:product_id])
      if (product)
        product.destroy
      end
    else
      Product.all.each do |product|
        product.destroy
      end
    end
  end
end
