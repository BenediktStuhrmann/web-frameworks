Rails.application.routes.draw do
  get 'products/show'
  get 'products/show_detail'
  get 'products/create'
  get 'products/modify'
  get 'products/delete'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
