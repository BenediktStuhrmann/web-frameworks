$(document).ready(function() {

  $("form#register").on("submit", function(e) {
    e.preventDefault();

    if ($("input#name").val() != "") {
      $("div.alert").addClass("d-none");
      this.submit();
    } else {
      $("div.alert").removeClass("d-none");
    }
  });

});
