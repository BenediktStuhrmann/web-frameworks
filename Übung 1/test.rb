###### Aufgabe 1

def ganzzahlige_teiler(von = 1, bis = 100)
  von.upto(bis){ |zahl|

    1.upto(zahl){ |teiler|
      if zahl.%(teiler) == 0
        print zahl, "->"
        puts teiler
      end
    }

    puts ""
  }
end

ganzzahlige_teiler 1, 20
puts ""


###### Aufgabe 2 ######

class String
  def printSpace
    self.each_char { |chr|
      print chr, " "
    }
  end
end

"Hallo".printSpace
puts ""
puts ""
puts ""


###### Aufgabe 3 ######

class Produkt

  def initialize(name, article_num, price, url)
    @name = name
    @article_num = article_num
    @price = price
    @url = url
  end

  attr_accessor :name, :article_num, :price, :url

end

produkt = Produkt.new("Produktname", 22, 33, "hallowelt")
print "Name: " + produkt.name + "\n"
print "Artikelnummer: ", produkt.article_num, "\n"
print "Preis: ", produkt.price
produkt.price= 44
print " -> ", produkt.price, "\n"
print "Url: " + produkt.url
puts ""


###### Aufgabe 4,5 ######

class Warenkorb

  def initialize
    @product_list = Hash.new
  end

  attr_reader:product_list

  def add_product(product)
    if !@product_list.has_key?(product) then
      @product_list[product] = 1
    else
      @product_list[product] += 1
    end
  end

  def total_price
    price_total = 0
    @product_list.each { |product, count|
      price_total += product.price * count
    }
    price_total
  end
end

cart = Warenkorb.new
cart.add_product(produkt)
cart.add_product(produkt)
cart.add_product(produkt)
produkt2 = Produkt.new("Produktname2", 1, 3, "tollOder")
cart.add_product(produkt2)

print "Warenkorbinhalt: ", cart.product_list, "\n"
print "Gesamtpreis: ", cart.total_price
