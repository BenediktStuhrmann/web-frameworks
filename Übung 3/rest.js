const restify = require("restify");
const restifyPlugins = require("restify-plugins");
const MongoClient = require("mongodb").MongoClient;

MongoClient.connect("mongodb://localhost:27017", function(err, client) {
  if(err) {
    console.log("Something went wrong!");
    console.log(err);
  } else {
    console.log("We are connected!");

    let studentsCollection = client.db("studentDB").collection("students");

    let server = restify.createServer();
    server.use(restifyPlugins.bodyParser());
    server.listen(3030, function() {
      console.log('%s listening at %s', server.name, server.url);
    });

    // HTTP-Methoden für die REST-Operationen
    server.post("/student", function (req, res, next) {
      // Insert new student
      console.log("POST: /student");
      console.log("Firstname: " + req.params.firstname);
      console.log("Lastname: " + req.params.lastname);
      console.log("Age: " + req.params.age);

      let newStudent = req.params;
      newStudent.age = parseInt(req.params.age);

      studentsCollection.insertOne(newStudent, function(err, result) {
        if (err) throw err;
        console.log("Inserted student into the database!");
        res.send(201, "Inserted student!");
      });

      return next();
    });

    server.post("/delete", function (req, res, next) {
      // Delete student
      console.log("POST: /delete");
      console.log("Firstname: " + req.params.firstname);
      console.log("Lastname: " + req.params.lastname);
      console.log("Age: " + req.params.age);

      let student = req.params;
      if (req.params.age) student.age = parseInt(req.params.age);

      studentsCollection.deleteOne(student, function(err, result) {
        if (err) throw err;
        console.log("Deleted student from the database!");
        res.send(204, "Deleted student!");
      });

      return next();
    });


    server.get("/student", function show(req, res, next) {
      // Get a student from db
      console.log("GET: /student");

      studentsCollection.find({}).toArray(function(err, result) {
        if (err) throw err;
        console.log("Got all students from database!");
        console.log(result);
        res.send(JSON.stringify(result));
      });

      return next();
    });

  }
});
