const http = require("http");

http.createServer(onRequest).listen(8000);

function onRequest(request, response) {
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.write("Hello World!");
  response.end();
}
