// timers
// I/O callbacks
// idle, prepare
// poll
// check
// close callbacks

const fs = require("fs");

// timers Phase
setTimeout(function () {
  timeOutFunction(1);
}, 100);
setTimeout(function () {
  timeOutFunction(2);
}, 3000);

readFile("test.txt", 1);
readFile("test.txt", 2);
readFile("test.txt", 3);
readFile("test.txt", 4);
readFile("test.txt", 5);



function logWithTime(logText, counter) {
  let time = new Date().toUTCString();
  console.log(time + ": " + logText + ", Counter: " + counter);
}

function immediateFunction(counter) {
  logWithTime("Immediate", counter);
}

function timeOutFunction(counter) {
  logWithTime("Timeout", counter);
}

function doHeavy() {
  let i = 0;
  while(i < 999999999) {
    i++;
  }
}

function readFile(fileName, counter) {
  let readStream= fs.createReadStream(fileName);

  readStream
    // I/O calbacks phase
    .on("open", function() {
      doHeavy();
      logWithTime("Open", counter);
    })
    // poll phase
    .on("data", function(chunk) {
      //console.log(chunk.toString());
      doHeavy();
      logWithTime("Data", counter);
      times++;
      if (times == 3) {
        // check Phase
        setImmediate(function () {
          immediateFunction(counter);
        });
        times = 0;
      }
    })
    // close calbacks Phase
    .on("end", function() {
      doHeavy();
      logWithTime("End", counter);
    })
    // close callbacks Phase
    .on("close", function() {
      doHeavy();
      logWithTime("Close", counter);
    })
    // I/O Phase
    .on("error", function(err) {
      console.log(err);
    });
}

var times = 0;
